import logo from './logo.svg';
import './App.css';
import { connect } from 'redux-bundler-react';
import keycloak from './keycloak';
import { ReactKeycloakProvider } from '@react-keycloak/web';
import { AppRouter } from './routes';
import { useLocation } from 'react-router-dom'
import ProtectedPage from './page/ProtectedPage';
const App = props => {
  return (
    <ReactKeycloakProvider authClient={keycloak}>
    <div className="App">
      <ProtectedPage />
    </div>
    </ReactKeycloakProvider>
  );
  }
export default App;
