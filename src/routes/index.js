import { useKeycloak } from '@react-keycloak/web';

import React from 'react';
import { BrowserRouter, Route, Switch, useLocation } from 'react-router-dom';

import Menu from '../page/Menu';
import HomePage from '../page/HomePage';
import { PrivateRoute } from '../utilities/PrivateRoute';
import ProtectedPage from '../page/ProtectedPage';


export const AppRouter = (props) => {
    const {keycloak, initialized} = useKeycloak();
    // const location = useLocation();
    // const { pathname } = location;
    // console.log(pathname);
    // if (!initialized) {
    //     return <h3>Loading ... !!!</h3>;
    // }
    return (<>
        <Menu />
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={HomePage} />
                <PrivateRoute roles={['RealmAdmin']} path="/protected" component={ProtectedPage} />
            </Switch>
        </BrowserRouter>
    </>
    );
};