import { useKeycloak } from '@react-keycloak/web';
import React from 'react';
import AuthorizedFunction from '../utilities/AuthorizedFunction';
import { useLocation } from 'react-router-dom';

const Menu = () => {
    const {keycloak, keycloakInitialized}  = useKeycloak();
    // let loc = useLocation();
    // console.log(loc);
    return (
        <ul>
            <li><a href="/">Home Page </a></li>
            {AuthorizedFunction(['RealmAdmin']) && <li><a href="/protected">ProtectedPage</a></li>}
            {keycloak && !keycloak.authenticated &&
                <li><a className="no-underline text-red-700" onClick={() => keycloak.login()}>Login</a></li>
            }

            {keycloak && keycloak.authenticated &&
                <li>
                    <a className="no-underline text-red-700" onClick={() => keycloak.logout()}>Logout ({
                        keycloak.tokenParsed.preferred_username
                    })</a>
                </li>
            }

        </ul>
    )
}

export default Menu