import logo from '../logo.svg';
import '../App.css';
import { connect } from 'redux-bundler-react';

const ProtectedPage = props => {
    const { doAdd, doSubtract, count } = props;
    return (
        
       <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          {count}
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
         Protected Page
        </a>
        <button className="flex justify-center items-center h-12 w-48 rounded bg-green-700 text-black" onClick={doAdd}>ADD</button>
        <button style={{background: 'red', borderRadius: 10, height: 50, width: 200, color: 'black' }} onClick={doSubtract}>SUBTRACT</button>
      </header>
    )
}

export default connect(
  'doAdd',
  'doSubtract',
  'selectCount',
   ProtectedPage
);