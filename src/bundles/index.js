import counterBundle from './counter_bundle';
import { composeBundles } from 'redux-bundler';

export default composeBundles(counterBundle);