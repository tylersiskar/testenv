export default {
    // the name becomes the reducer name in the resulting state
    name: 'counter',
    // the Redux reducer function
    reducer:(state = {count: 0}, { type, payload }) => {
        console.log(type, 'reducer', state);
          switch (type) {
            case 'ADD':
              return{...state, count: state.count + 1 };
            case 'SUBTRACT':
              return{...state, count: state.count - 1 };
            default:
              return state;
          }
    }
      ,
    // anything that starts with `select` is treated as a selector
    // selectors get full state object so they can use state from other bundles
    selectCount: state => state.counter.count,
    // anything that starts with `do` is treated as an action creator
    doAdd: () => ({ dispatch, store }) => {
      console.log(store);
      return dispatch({ type: 'ADD' });
    },
    doSubtract: () => ({ dispatch }) => dispatch({ type: 'SUBTRACT'})
  }